import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n;
        int x;
        int y;
        int program;

        System.out.println("Welcome to this queen-solver!");
        System.out.println("This solver has two different methods for solving queen-puzzles");
        System.out.println("Choose method 1 to use backtracking, which is guaranteed to give you a solution if one exists, but runs slowly");
        System.out.println("Choose method 2 to use conflictminimization. This is a lot faster, but is not guaranteed to give you a solution, as it has a fragment of randomisation");
        System.out.print("Choose method: ");
        program = Integer.parseInt(scanner.nextLine());
        System.out.println("Choose how big your chess board is. I would advice against going higher than 15.");
        System.out.print("Board size: ");
        n = Integer.parseInt(scanner.nextLine());
        if (n == 8) {
            System.out.println("Choose the position of the first queen in algebraic notation");
            System.out.print("Position: ");
            String position = scanner.nextLine();
            x = 7-(Integer.parseInt(String.valueOf(position.charAt(1))) - 1);
            y = ((int) position.charAt(0) - 65);
            System.out.println(x);
            System.out.println(y);
        } else {
            System.out.print("Choose the x-coordinate (1-indexed) for your first queen: ");
            x = Integer.parseInt(scanner.nextLine()) - 1;
            System.out.print("Choose the y-coordinate (1-indexed) for your first queen: ");
            y = Integer.parseInt(scanner.nextLine()) - 1;
        }

        Main main = new Main();
        if (program==1) main.checkCoordinates(n, x, y);
        else if (program==2) main.heuristicsCheck(n, x, y);
        else {
            System.out.println("Seems you didn't choose method correctly!");
        }
    }

    private void checkCoordinates(int n, int x, int y) {
        if (x < 0 || y < 0 || x > n || y > n) {
            System.out.println("Not possible. Try a new configuration");
            System.exit(1);
        }
        Board board = new Board(n);
        Board solvedBoard = board.solveBoard(x, y);
        if (solvedBoard.getBoard() == null) {
            System.out.println("This was an impossible starting configuration!");
        } else for (int i = 0; i < solvedBoard.getBoard().length; i++) {
            System.out.println(Arrays.toString(solvedBoard.getBoard()[i]));
        }
    }

    private void heuristicsCheck(int n, int x, int y) {
        if (x < 0 || y < 0 || x > n || y > n) {
            System.out.println("Not possible. Try a new configuration");
            System.exit(1);
        }
        HeuristicBoard board = new HeuristicBoard(n);
        HeuristicBoard solvedBoard = board.solveBoard(x, y);
        if (solvedBoard.getBoard() == null) {
            System.out.println("This was an impossible starting configuration or I got stuck in a local minimum. You could try again!");
        } else for (int i = 0; i < solvedBoard.getBoard().length; i++) {
            System.out.println(Arrays.toString(solvedBoard.getBoard()[i]));
        }

    }
}
