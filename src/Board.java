import java.util.Arrays;

public class Board {
    private String[][] board;
    public Board(int n) {
        this.board = new String[n][n];
        for (String[] row : this.board) {
            Arrays.fill(row, "o");
        }
    }

    public String[][] getBoard() {
        return board;
    }

    public Board(Board board, int x, int y) {
        String[][] originalBoard = board.getBoard();
        int n = originalBoard.length;
        this.board = new String[n][n];
        if (originalBoard[x][y] != "o") {
            this.board = null;
        } else {
            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    this.board[i][j] = originalBoard[i][j];
                }
            }
            this.addQueen(x, y);
        }
    }

    private void addQueen(int x, int y) {
        this.board[x][y] = "Q";
        int n = this.board.length;
        int tempX = x;
        int tempY = y;
        tempX++;
        tempY++;
        while (tempX < n && tempY < n) {
            this.board[tempX][tempY] = "X";
            tempX++;
            tempY++;
        }
        tempX = x;
        tempY = y;
        tempX++;
        while (tempX < n) {
            this.board[tempX][tempY] = "X";
            tempX++;
        }
        tempX = x;
        tempY = y;
        tempX++;
        tempY--;
        while (tempX < n && tempY >= 0) {
            this.board[tempX][tempY] = "X";
            tempX++;
            tempY--;
        }
        tempX = x;
        tempY = y;
        tempX--;
        while (tempX >= 0) {
            this.board[tempX][tempY] = "X";
            tempX--;
        }
        tempX = x;
        tempY = y;
        tempX--;
        tempY++;
        while (tempX >= 0 && tempY < n) {
            this.board[tempX][tempY] = "X";
            tempX--;
            tempY++;
        }
        tempX = x;
        tempY = y;
        tempX--;
        tempY--;
        while (tempX >= 0 && tempY >= 0) {
            this.board[tempX][tempY] = "X";
            tempX--;
            tempY--;
        }
        tempX = x;
        tempY = y;
        tempY--;
        while (tempY >= 0) {
            this.board[tempX][tempY] = "X";
            tempY--;
        }
        tempX = x;
        tempY = y;
        tempY++;
        while (tempY < n) {
            this.board[tempX][tempY] = "X";
            tempY++;
        }

    }


    public Board solveBoard(int x, int y) {
        this.addQueen(x, y);
        return this.checkBoard();
    }

    public Board checkBoard() {
        for (int i = 0; i < this.board.length; i++) {
            boolean foundQueen = false;
            for (int j = 0; j < this.board[i].length; j++) {
                if (this.board[i][j].equals("Q")) {
                    foundQueen = true;
                }
            }
            if (foundQueen) continue;
            else {
                for (int j = 0; j < this.board[i].length; j++) {
                    Board newBoard = new Board(this, i, j);
                    if (newBoard.board == null) {
                        continue;
                    }
                    if (newBoard.checkBoard().getBoard() != null) {
                        return newBoard.checkBoard();
                    }
                }
                this.board = null;
                return this;
            }
        }
        return this;
    }

    public Board solveBoardByHeuristics(int x, int y) {

        return this;
    }



    public boolean checkIfNoCollisions() {
        return true;
    }
}
