public class Queen {
    private int conflicts;
    private int x;
    private int y;
    private boolean movable;

    public Queen(int x, int y) {
        this.x = x;
        this.y = y;
        this.conflicts = 0;
        this.movable = true;
    }

    public void setMovable(boolean movable) {
        this.movable = movable;
    }

    public boolean isMovable() {
        return movable;
    }

    public int getConflicts() {
        return conflicts;
    }

    public void setConflicts(int conflicts) {
        this.conflicts = conflicts;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
