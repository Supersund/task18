import java.util.Scanner;

public class EightQueensProblem {
    public static void main(String[] args) {
        ChessBoard b = new ChessBoard();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Welcome to my solution of the Eight Queens Problem");
        System.out.println("Where do you want to place the queen?");
        String pos = scanner.nextLine();
        int y = ((int) pos.charAt(0) -65);
        int x = 7 - (Integer.parseInt(String.valueOf(pos.charAt(1)))-1);
        scanner.close();

        b.initBoard(x,y);
        ChessBoard solution = b.findSolutions();

        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                System.out.print(solution.board[i][j] + "  ");
            }
            System.out.println("\n");
        }
    }
}