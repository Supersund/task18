import java.util.Arrays;

public class ChessBoard {
    String queen;
    String illegalPos;
    String[][] board;

    public ChessBoard() {
        this.queen = "Q";
        this.illegalPos = "X";
        this.board = new String[8][8];
        for (String[] row : this.board) {
            Arrays.fill(row, "O");
        }
    }

    public ChessBoard(ChessBoard b, int x, int y) {
        String[][] originalBoard = b.board;
        this.board = new String[8][8];
        this.illegalPos = "X";
        this.queen = "Q";
        if (originalBoard[x][y] != "O")
            this.board = null;
        else {
            for (int i = 0; i < 8; i++) {
                for (int j = 0; j < 8; j++) {
                    this.board[i][j] = originalBoard[i][j];
                }
            }
            // System.out.println("Før queen: " +board[0][0]);
            this.addQueen(x, y);
            // System.out.println("Etter queen: " +board[0][0]);
        }
    }

    private void addQueen(int x, int y) {

        // Sets all the spaces this queen covers to "X"
        for (int i = 0; i < 8; i++) {
            board[x][i] = illegalPos;
            board[i][y] = illegalPos;

            if (!(x - i < 0) && !(y - i < 0)) {
                board[x - i][y - i] = illegalPos;
            }
            if (!(x - i < 0) && !(y + i >= 8)) {
                board[x - i][y + i] = illegalPos;
            }
            if (!(x + i >= 8) && !(y - i < 0)) {
                board[x + i][y - i] = illegalPos;
            }
            if (!(x + i >= 8) && !(y + i >= 8)) {
                board[x + i][y + i] = illegalPos;
            }
        }
        this.board[x][y] = queen;
    }

    public void initBoard(int x, int y) {
        addQueen(x, y);
    }

    public ChessBoard findSolutions() {
        for (int i = 0; i < 8; i++) {
            Boolean queenInRow = false;
            for (int j = 0; j < 8; j++) {
                if (this.board[i][j].equals("Q")) {
                    queenInRow = true;
                }
            }
            if (queenInRow) {
                continue;
            } else {
                for (int j = 0; j < 8; j++) {
                    ChessBoard newBoard = new ChessBoard(this, i, j);
                    if (newBoard.board == null) {
                        continue;
                    }
                    if (newBoard.findSolutions().board != null) {
                        return newBoard.findSolutions();
                    }
                }
                this.board = null;
                return this;
            }
        }
        return this;
    }
}