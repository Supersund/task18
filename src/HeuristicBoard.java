import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class HeuristicBoard {
    private String[][] board;
    private Queen[] queens;
    private int boardSize;

    public HeuristicBoard(int n) {
        this.boardSize = n;
        this.queens = new Queen[n];
        this.board = new String[n][n];
        for (String[] row : this.board) {
            Arrays.fill(row, "o");
        }
    }

    private void initializeBoard(int x, int y) {
        Random r = new Random();
        this.queens[x] = new Queen(x, y);
        this.queens[x].setMovable(false);
        for (int i = 0; i < this.boardSize; i++) {
            if (i == x) continue;
            this.queens[i] = new Queen(i, r.nextInt(this.boardSize));
        }
        for (Queen queen : this.queens) {
            this.board[queen.getX()][queen.getY()] = "Q";
        }
    }

    public HeuristicBoard solveBoard(int x, int y) {
        initializeBoard(x, y);
        System.out.println(getNumberOfConflicts(x, y));
        if (!this.findMove()) this.board = null;
        return this;
    }

    private void moveQueen(Queen queen, int x, int y) {
        this.board[queen.getX()][queen.getY()] = "o";
        this.board[x][y] = "Q";
        queen.setX(x);
        queen.setY(y);
    }

    private boolean findMove() {
        int steps = 0;
        int maxConflicts = 0;
        ArrayList<Queen> maxConflictQueens = new ArrayList<>();
        while (steps < 5000) {
            steps++;
            maxConflicts = 0;
            for (Queen queen : this.queens) {
                int conflicts = getNumberOfConflicts(queen.getX(), queen.getY());
                queen.setConflicts(conflicts);
                if (conflicts == maxConflicts && queen.isMovable()) {
                    maxConflictQueens.add(queen);
                } else if (conflicts > maxConflicts && queen.isMovable()) {
                    maxConflicts = conflicts;
                    maxConflictQueens = new ArrayList<>();
                    maxConflictQueens.add(queen);
                }
            }
            if (maxConflicts == 0) return true;
            else {
                Queen queenToBeMoved = maxConflictQueens.get(new Random().nextInt(maxConflictQueens.size()));
                int minCollisions = this.boardSize;
                ArrayList<Integer> bestSquares = new ArrayList<Integer>();
                for (int i = 0; i < this.boardSize; i++) {
                    int collisions = this.getNumberOfConflicts(queenToBeMoved.getX(), i);
                    if (i != queenToBeMoved.getY()) collisions--;
                    if (collisions == minCollisions) {
                        bestSquares.add(i);
                    } else if (collisions < minCollisions) {
                        bestSquares = new ArrayList<>();
                        bestSquares.add(i);
                        minCollisions = collisions;
                    }
                }
                moveQueen(queenToBeMoved, queenToBeMoved.getX(), bestSquares.get(new Random().nextInt(bestSquares.size())));
            }

        }
        return false;
    }

    public String[][] getBoard() {
        return board;
    }

    private int getNumberOfConflicts(int x, int y) {
        int conflicts = 0;
        int n = this.board.length;
        /*
        Returns the number of conflicts that a square has
         */
        int tempX = x;
        int tempY = y;
        tempX++;
        tempY++;
        while (tempX < n && tempY < n) {
            if (this.board[tempX][tempY].equals("Q")) conflicts++;
            tempX++;
            tempY++;
        }
        tempX = x;
        tempY = y;
        tempX++;
        while (tempX < n) {
            if (this.board[tempX][tempY].equals("Q")) conflicts++;
            tempX++;
        }
        tempX = x;
        tempY = y;
        tempX++;
        tempY--;
        while (tempX < n && tempY >= 0) {
            if (this.board[tempX][tempY].equals("Q")) conflicts++;
            tempX++;
            tempY--;
        }
        tempX = x;
        tempY = y;
        tempX--;
        while (tempX >= 0) {
            if (this.board[tempX][tempY].equals("Q")) conflicts++;
            tempX--;
        }
        tempX = x;
        tempY = y;
        tempX--;
        tempY++;
        while (tempX >= 0 && tempY < n) {
            if (this.board[tempX][tempY].equals("Q")) conflicts++;
            tempX--;
            tempY++;
        }
        tempX = x;
        tempY = y;
        tempX--;
        tempY--;
        while (tempX >= 0 && tempY >= 0) {
            if (this.board[tempX][tempY].equals("Q")) conflicts++;
            tempX--;
            tempY--;
        }
        tempX = x;
        tempY = y;
        tempY--;
        while (tempY >= 0) {
            if (this.board[tempX][tempY].equals("Q")) conflicts++;
            tempY--;
        }
        tempX = x;
        tempY = y;
        tempY++;
        while (tempY < n) {
            if (this.board[tempX][tempY].equals("Q")) conflicts++;
            tempY++;
        }

        return conflicts;
    }
}
